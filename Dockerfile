FROM nikolaik/python-nodejs:python3.10-nodejs17-slim
RUN apt update -y

COPY . /app
WORKDIR /app

# Setting up the backend
RUN pip install -r dev-requirements.txt

# Setting up the frontend
RUN yarn install

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1
