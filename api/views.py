
from rest_framework.decorators import api_view
from rest_framework.response import Response

@api_view()
def hello_world(request) -> Response:
    foo: int = 2 + 2
    return Response({"message": "Hello, world!"})
