from typing import Any, Callable, List, Union

from django.urls import URLPattern, URLResolver, path

from . import views

urlpatterns: List[Union[URLPattern, URLResolver]] = [
    path('foobar/', views.hello_world),
]
