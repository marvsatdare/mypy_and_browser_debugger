# Marvs Demo

## Frontend

To get started run: `yarn install`

To run developmental server:

- `yarn start`

Runs the app in the development mode.\
Open [http://localhost:1234](http://localhost:1234) to view it in your browser. The page will reload when you make changes. You may also see any lint errors in the console.

To build for production:

- `yarn build`

You will now need to find a way to serve the output of the build process

## Backend

To get started run: `pip install -r dev-requirements.txt`. After this run migrations `python manage.py migrate`.

To run developmental server:

- `python manage.py runserver`

Runs the app in the development mode.\
Open [http://localhost:8000](http://localhost:8000) to view it in your browser. The page will reload when you make changes. You may also see any lint errors in the console.

## Docker

To spin up containers for both frontend and backend:

- `docker-compose up --build`

## Mypy

Typings was introduce in Python 3.5 - `https://docs.python.org/3/library/typing.html`.

Mypy is a static type checker for Python 3 and Python 2.7. If you sprinkle your code with type annotations, mypy can type check your code and find common bugs. As mypy is a static analyzer, or a lint-like tool, the type annotations are just hints for mypy and don’t interfere when running your program. You run your program with a standard Python interpreter, and the annotations are treated effectively as comments.

To run mypy `mypy {directory_you_want_to_check_againist}`

```python

# Potential ways to use mypy
# In a method
Class FooBar:
    def special_method(foo: str, bar: int) -> None:
        # We now know for sure that foo is a string so the dev that did this needs telling off..
        foobar = foo / 2

        # We now know for sure that bar is an interger so this should work
        example_2 = bar / 2 


# In a function
def special_func(foo: str, bar: int) -> None:
    # We now know for sure that foo is a string so the dev that did this needs telling off...
    foobar = foo / 2

    #  We now know for sure that bar is an interger so this should work
    example_2 = bar / 2

# With varibles
foo: string = ''
bar: int = 2

# More advance examples - https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html
from typing import List, Set, Dict, Tuple, Optional

# For simple built-in types, just use the name of the type
x: int = 1
x: float = 1.0
x: bool = True
x: str = "test"
x: bytes = b"test"

# For collections, the type of the collection item is in brackets
# (Python 3.9+)
x: list[int] = [1]
x: set[int] = {6, 7}

# In Python 3.8 and earlier, the name of the collection type is
# capitalized, and the type is imported from the 'typing' module
x: List[int] = [1]
x: Set[int] = {6, 7}

# Same as above, but with type comment syntax (Python 3.5 and earlier)
x = [1]  # type: List[int]

# For mappings, we need the types of both keys and values
x: dict[str, float] = {"field": 2.0}  # Python 3.9+
x: Dict[str, float] = {"field": 2.0}

# For tuples of fixed size, we specify the types of all the elements
x: tuple[int, str, float] = (3, "yes", 7.5)  # Python 3.9+
x: Tuple[int, str, float] = (3, "yes", 7.5)

# For tuples of variable size, we use one type and ellipsis
x: tuple[int, ...] = (1, 2, 3)  # Python 3.9+
x: Tuple[int, ...] = (1, 2, 3)

# Use Optional[] for values that could be None
x: Optional[str] = some_function()

# Mypy understands a value can't be None in an if-statement
if x is not None:
    print(x.upper())

# If a value can never be None due to some invariants, use an assert
assert x is not None
print(x.upper())

def foobar():
    print("foobar")

a: Callable = foobar

```

## Mypy (Exisiting Codebases)

`https://mypy.readthedocs.io/en/stable/existing_code.html`

## Dmypy

`https://mypy.readthedocs.io/en/stable/mypy_daemon.html`

## VSCode Browser Debuggers

Chrome - `https://github.com/microsoft/vscode-js-debug#readme`
Firefox - `https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug`

The code for the browser debuggers can be found in .vscode/. It should be pretty much copy and paste for parcel projects. The most important thing here is:

```js
"sourceMapPathOverrides": {
    "/__parcel_source_root/*": "${webRoot}/*"
},
```

OR

```js
"pathMappings": [
    {
        "url": "file:///__parcel_source_root/src",
        "path": "${workspaceFolder}/src"
    }
],
```

This maps the code in the project to the code in presented in the browser and helps the debugger know where you are setting the breakpoint. The example here is for parcel but it should be something similar for webpack. I wanted to have an example for both but did not have the chance however here is a good starting point :

- `https://www.jbssolutions.com/resources/blog/debugging-webpack-build-scripts-and-app-scripts-vscode-part-2/` 
- `https://github.com/kube/vscode-ts-webpack-node-debug-example`
- `https://curia.me/how-to-configure-a-typescript-webpack-project-in-visual-studio-code/`
- `https://vscode-debug-specs.github.io/javascript_chrome/`

At some point I may come back and add a webpack example.

## Python Debugger

- `https://code.visualstudio.com/docs/python/debugging`
- `https://code.visualstudio.com/docs/containers/debug-python`
- `https://testdriven.io/blog/django-debugging-vs-code/`
