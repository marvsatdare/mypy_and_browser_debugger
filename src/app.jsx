import React, { useEffect } from 'react';

export default function App() {
  const consoleLog = () => {
    console.log('To the console');
  };

  const bigBrainMaths = 2 + 2;
  const fooBar = 6 + 4;
  console.log(bigBrainMaths, fooBar);

  useEffect(() => {
    consoleLog();
  }, []);

  return (
    <div>
      <h1>Hello. This is parcel react app</h1>
    </div>
  );
}

let a = 2;
let b = 3;